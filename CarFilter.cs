﻿using System.Collections.Generic;
using System.Linq;
using CarsFilter.Enums;

namespace CarsFilter
{
    public class CarFilter
    {
        public List<Car> FilterByType(IEnumerable<Car> cars, CarType type) =>
            cars.Where(c => c.Type == type).ToList();
        public List<Car> FilterByType(IEnumerable<Car> cars, CarType type) =>
            cars.Where(c => c.Color == color).ToList();
        public List<Car> FilterByType(IEnumerable<Car>,Color color, CarType type) =>
            cars.Where(c => c.Color == color&& c.Type==type).ToList();
    }
}
