﻿using System;

namespace UserCreation
{
    class Program
    {
        static void Main(s)
        {
            Console.WriteLine("Welcome to user creation app!");

            var person = new Person();

            Console.WriteLine("Enter your first name:");
            person.FirstName = Console.ReadLine();

            Console.WriteLine("Enter your last name:");
            person.LastName = Console.ReadLine();
            var user = new User(person);
            user.OutputUserName();

            Console.WriteLine("Press enter to exit...");
        }
        public static string ReadUserInput()
        {
            string returnvalue = null;
            while (string.IsNullOrWhiteSpace(returnvalue))
            {
                Console.WriteLine("Press enter!");
                returnvalue = Console.ReadLine();
            }
            return returnvalue;
}
           
        }
    }

